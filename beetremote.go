package main

import (
    "fmt"
    "flag"
    "net"
    "net/url"
    "net/http"
    "strings"
    "os/exec"
    "github.com/gorilla/websocket"
    "github.com/gobuffalo/packr"
)

var upgrader = websocket.Upgrader{}

var commands = map[string][]byte{
    "prev": []byte("playlist-prev\n"),
    "next": []byte("playlist-next\n"),
    "pause": []byte("cycle pause\n"),
    "stop": []byte("stop\n"),
    "forward": []byte("seek +10\n"),
    "back": []byte("seek -10\n"),
    "volup": []byte("add volume +10\n"),
    "voldown": []byte("add volume -10\n"),
    "mute": []byte("cycle mute\n"),
    "info": []byte("{\"command\":[\"get_property_string\", \"metadata\"]}\n"),
    "muted": []byte("{\"command\":[\"get_property_string\", \"mute\"]}\n"),
    "volume": []byte("{\"command\":[\"get_property_string\", \"volume\"]}\n"),
}

func emitResponse(sock net.Conn, conn *websocket.Conn) {
    usbuffer := make([]byte, 4096)
    for {
        n, err := sock.Read(usbuffer[:])
        if err != nil {
            conn.Close()
            return
        }
        err = conn.WriteMessage(websocket.TextMessage, usbuffer[0:n])
        if err != nil {
            break
        }
    }
}

func socketHandler(sockpath string) func(res http.ResponseWriter, req *http.Request) {
        return func(res http.ResponseWriter, req *http.Request) {
        conn, err := upgrader.Upgrade(res, req, nil)
        if err != nil {
            return
        }
        defer conn.Close()
        sock, err := net.Dial("unix", sockpath)
        if err != nil {
            return
        }
        defer sock.Close()
        go emitResponse(sock, conn)
        for {
            _, message, err := conn.ReadMessage()
    		if err != nil {
    			break
    		}
            sock.Write(commands[string(message)])
        }
    }
}

func dialAndSend(cmd string, sockpath string) {
    sock, err := net.Dial("unix", sockpath)
    if err != nil {
        return
    }
    defer sock.Close()
    sock.Write(commands[cmd])
}

func playQuery(sockpath string) func(res http.ResponseWriter, req *http.Request) {
    return func(res http.ResponseWriter, req *http.Request) {
        dialAndSend("stop", sockpath)
        query, _ := url.PathUnescape(strings.Split(req.URL.Path, "/")[2])
        out, err := exec.Command("beet", strings.Split("play -y "+query, " ")...).Output()
        fmt.Println(string(out), err)
    }
}

func main() {
    port := flag.String("p", "8337", "port")
    sockpath := flag.String("s", "/tmp/beetsocket", "mpv socket path")
    staticBox := packr.NewBox("./static")
    http.HandleFunc("/query/", playQuery(*sockpath))
    http.HandleFunc("/ws", socketHandler(*sockpath))
    http.Handle("/", http.FileServer(staticBox))
    http.ListenAndServe(":"+*port, nil)
}
