# beetremote

WIP. Simple app for remote controlling `beet play` from a mobile browser.
Requires [beets](https://beets.io/) configured to play with mpv.

![](./beetremote.png)
