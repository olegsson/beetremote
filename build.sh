#/usr/bin/env bash

if packr; then
    go install beetremote.go
else
    go get -u github.com/gobuffalo/packr/packr
    ./build.sh
fi
