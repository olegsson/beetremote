function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

function connect() {

    let sock = new WebSocket(`ws://${window.location.host}/ws`);

    let queryStatus = () => ['info', 'muted', 'volume'].map((q) => sock.send(q));
    let poller;

    sock.onopen = () => {
        queryStatus();
        poller = setInterval(queryStatus, 200);
        if (sock.readyState === 1) document.getElementById('stop').classList.remove('active');
    };

    sock.onclose = async () => {
        clearInterval(poller);
        [
            'pause',
            'mute',
        ].map((id) => document.getElementById(id).classList.remove('active'));
        document.getElementById('media-artist').innerHTML = 'connecting...';
        [
            'media-title',
            'media-album',
            'volume',
        ].map((id) => document.getElementById(id).innerHTML = '')
        document.getElementById('stop').classList.add('active');
        await sleep(1000);
        connect();
    };

    sock.onmessage = (message) => {
        try {
            let data = JSON.parse(message.data.split('\n')[0]);

            switch (true) {
                case data.event !== undefined:
                    switch(data.event) {
                        case 'pause':
                            document.getElementById('pause').classList.add('active');
                            break;

                        case 'unpause':
                            document.getElementById('pause').classList.remove('active');
                            break;

                        default:
                            break;
                    }
                    break;

                case data.data === 'yes':
                    document.getElementById('mute').classList.add('active');
                    break;

                case data.data === 'no':
                    document.getElementById('mute').classList.remove('active');
                    break;

                case !isNaN(data.data):
                    document.getElementById('volume').innerHTML = `Vol. ${parseInt(data.data)}%`;
                    break;

                case ![undefined, null].includes(data.data):
                    [
                        'media-artist',
                        'media-title',
                        'media-album',
                    ].map((id) => document.getElementById(id).innerHTML = '')
                    try {
                        let info = JSON.parse(data.data.toLowerCase());
                        document.getElementById('media-artist').innerHTML = info.artist.toUpperCase();
                        document.getElementById('media-title').innerHTML = info.title.toUpperCase();
                        document.getElementById('media-album').innerHTML = info.album.toUpperCase();
                    } catch(err) {
                        console.log(err);
                    }
                    break;

                default:
                    break;
            }
        } catch(err) {
            console.log(err, message.data);
        }
    };

    [
        'prev',
        'pause',
        'next',
        'stop',
        'forward',
        'back',
        'volup',
        'voldown',
        'mute',
    ].map((cmd) => {
        document.getElementById(cmd).onclick = () => sock.send(cmd);
    });

}

document.getElementById('play').onclick = () => {
    let query = encodeURI(document.getElementById('query').value);
    fetch('./query/'+query)
};

connect();
